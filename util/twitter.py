#!/usr/bin/python3
from twitter.oauth import OAuth
from keys import *
from urllib.error import URLError
from http.client import BadStatusLine

def oauth_login():
    '''Returns an OAuth instance'''
    return OAuth(access_tok, access_sec, consumer_key, consumer_sec)

class Tweeter:
    ''' A class for storing useful information about Twitter users '''
    def __init__(self, name, uid, fr_count, fo_count):
        self.name = name
        self.uid = uid
        self.fr_count = fr_count
        self.fo_count = fo_count

    # Below functions for compatibility with NetworkX
    def __hash__(self):
        return self.uid

    def __eq__(self, other):
        return self.uid == other

    def __repr__(self):
        return "Tweeter(name=%r, uid=%r, fr_count=%r, fo_count=%r)" %\
                (self.name, self.uid, self.fr_count, self.fo_count)

    def __str__(self):
        return "@" + self.name

def get_info(tweet):
    """Return relevant information about a tweet, whether normal or retweet"""
    if tweet.get('retweeted_status'):
        return get_info(tweet['retweeted_status'])
    if tweet['truncated']:
        return (tweet['id'], tweet['extended_tweet']['full_text'], tweet['retweet_count'])
    return (tweet['id'], tweet['text'], tweet['retweet_count'])

error_sep = '*'*50
def make_twitter_request(func, max_errors=10, *args, **kwargs):
    ''' 
    A wrapper for handling Twitter requests, errors and rate limits 
    Credit to TwitterCookbook
    '''
    def handle_twitter_http_error(e, wait_period=2, sleep_when_rate_limited=True):
        if wait_period > 3600: # seconds
            print('Too many retries. Quitting', file=sys.stderr)
            raise e
        if e.e.code == 401:
            print(error_sep)
            print('* Encountered 401 Error (Not Authorized)', file=sys.stderr)
            print(error_sep)
            return None
        elif e.e.code == 404:
            print(error_sep)
            print('* Encountered 404 Error (Not Found)', file=sys.stderr)
            print(error_sep)
            return None
        elif e.e.code == 429:
            if sleep_when_rate_limited:
                if term: start_countdown()
                print(error_sep)
                print('* Encountered 429 Error (Rate Limit Exceeded)', file=sys.stderr)
                print('* Current time: {}'.format(time.strftime('%T')))
                print('* Retrying in 15 minutes...ZzZ...', file=sys.stderr)
                sys.stderr.flush()
                time.sleep(60*15 + 5)
                print('* ...ZzZ...Awake now and retrying.', file=sys.stderr)
                print(error_sep)
                return 2
            else:
                raise e # Caller must handle the rate limiting issue
                print(error_sep)
        elif e.e.code in (500, 502, 502, 504):
            print(error_sep)
            print('* Encountered {0} Error. Retrying in {1} seconds'.format(e.e.code, wait_period), file=sys.stderr)
            time.sleep(wait_period)
            wait_period *= 1.5
            return wait_period
        else:
            raise e
    # End of http error handler

    wait_period = 2
    error_count = 0

    while True:
        try:
            return func(*args, **kwargs)
        except twitter.api.TwitterHTTPError as e:
            error_count = 0
            wait_period = handle_twitter_http_error(e, wait_period)
            if wait_period is None:
                return
        except URLError as e:
            error_count += 1
            time.sleep(wait_period)
            wait_period *= 1.5
            print('URLError encountered. Continuing.', file=sys.stderr)
            if error_count > max_errors:
                print('Too many consecutive errors...bailing out.', file=sys.stderr)
                raise
        except BadStatusLine as e:
            error_count += 1
            time.sleep(wait_period)
            wait_period *= 1.5
            print('BadStatusLine encountered. Continuing.', file=sys.stderr)
            if error_count > max_errors:
                print('Too many consecutive errors...bailing out.', file=sys.stderr)
                raise
