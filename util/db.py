#!/usr/bin/python3
import sqlite3
from sys import stderr
from pathlib import Path

#TODO:
# - look into changing row for easier sentiment analysis (return tuple of id, content)
# - maybe create a Tweet object for reading into/writing out of database?
# - different tables for different candidates, one db per session
# - stuff in __exit__ for exceptions: https://docs.python.org/2/reference/datamodel.html#object.__enter__
# - general exception handling
#   - graceful shutdown (take care of DB closing)
#   - handle failed transactions
# - look into BEGIN/END TRANSACTION for writing transactions

class QuietException(Exception):
    pass

class TweetDB:
    """A class for handling reading/writing tweets to a database"""

    # Class methods
    def __init__(self, db="./util/test.db", debug=False):
        """Creates a database for storing tweets"""
        self._name = db
        self._debug = debug
        self._db = sqlite3.connect(self._name)
        self.connected = True
        try:
            self.list_tables()
        except sqlite3.DatabaseError as e:
            print(f"!! Error opening \"{db}\"", file=stderr)
            print(f"!! DatabaseError: {e.args[0]}", file=stderr)
            self.connected = None
            return
        self._db.row_factory = sqlite3.Row
        self.close()

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.commit()
        self.close()

    # General DB methods
    def connect(self):
        """Connects to the database if not already"""
        if self.connected: return
        self._db = sqlite3.connect(self._name)
        self.connected = True

    def commit(self):
        """Commits pending changes to the db"""
        self._db.commit()

    def close(self):
        """Closes the database"""
        if not self.connected: return
        self._db.close()
        self.connected = False

    def do(self, stmnt, params=None, keep=True):
        """Executes a generic SQL statement"""
        if self._debug: print(f"<DEBUG> self.do: stmnt = {stmnt}")
        if keep: # Don't close database afterwards
            self.connect()
            return self._db.execute(stmnt, params or ())
        with self:
            r = self._db.execute(stmnt, params or ())
        return r

    def do_many(self, stmnt, params, keep=True):
        """Execute a statement against a list of parameters"""
        if self._debug: print(f"<DEBUG> self.do_many: stmnt = {stmnt}, params = {params}")
        if keep:
            self.connect()
            return self._db.executemany(stmnt, params)
        with self:
            return self._db.executemany(stmnt, params)

    # DB methods for writing data
    def add_table(self, table):
        """Creates a tweet table with the specified name"""
        stmnt = f"CREATE TABLE IF NOT EXISTS {table} ("\
            "tid INT NOT NULL PRIMARY KEY, "\
            "content TEXT NOT NULL, "\
            "count INT NOT NULL, "\
            "senti REAL NOT NULL)"
        self.do(stmnt, keep=False)

    def add(self, table, tid, content, count=0, senti=0):
        """Inserts a tweet into the db"""
        self.do(f"INSERT INTO {table} VALUES (?, ?, ?, ?)", (tid, content, count, senti), keep=False)

    def indate_tweet(self, table, tid, content, count=0, senti=0):
        """Either __in__sert new tweet or up__date__ retweet count if exists"""
        # Because ids and text shouldn't change,
        # and senti is calculated after the tweet is in the db
        stmnt = f"INSERT INTO {table} "\
                "VALUES (:tid, :con, :count, :senti) "\
                "ON CONFLICT (tid) "\
                "DO UPDATE SET count=:count"
        vals = {'tid':tid, 'con':content, 'count':count, 'senti':senti}
        self.do(stmnt, vals, keep=False)

    def update_senti(self, table, tid, senti, keep=False):
        """Update the sentiment score for a tweet"""
        self.do(f"UPDATE {table} SET senti=? WHERE tid=?", (senti, tid), keep=keep)

    def batch_senti(self, table, vals, keep=True):
        """Batch update senitment values"""
        self.do_many(f"UPDATE {table} SET senti=? WHERE tid=?", vals, keep=keep)

    def delete(self, table, where=None):
        """Deletes all entries in the table"""
        if not where:
            self.do(f"DELETE FROM {table}", keep=False)
        else:
            self.do(f"DELETE FROM {table} WHERE {where}", keep=False)

    def drop(self, table):
        """Drops the specified table"""
        self.do(f"DROP TABLE IF EXISTS {table}", keep=False)

    # DB methods for reading data
    #   Database needs to be closed manually afterwards
    def list_tables(self):
        """Returns a list of available tables"""
        return list(self.do("SELECT name FROM sqlite_master WHERE type='table'").fetchall()[0])

    def count(self, table):
        """Returns a count of the rows in the specified table"""
        return self.do(f"SELECT count(*) FROM {table}").fetchone()[0]

    def sum(self, table, col):
        """Returns the sum of the selected column"""
        pass

    def read_all(self, table):
        """Returns a cursor of all the rows in the table"""
        return self.do(f"SELECT * FROM {table}")

    def read_chunk(self, table, size=100):
        """Returns a generator of $size rows at a time"""
        r =  self.do(f"SELECT * FROM {table}")
        chunk = r.fetchmany(size=size)
        while len(chunk) > 0:
            yield chunk
            chunk = r.fetchmany(size=size)
