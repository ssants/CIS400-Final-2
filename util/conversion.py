#!/usr/bin/python3
import pickle
import networkx as nx

# Graph import/export functions
def graph2bin(nodes):
    '''Store a graph into a file'''
    with open('nodes.bin', 'wb') as f:
        pickle.dump(nodes, f)

def bin2graph(filename):
    '''Get a graph from a file'''
    with open(filename, 'rb') as f:
        nodes = pickle.loads(f.read())
    return nx.DiGraph(nodes)

def bin2dict(filename):
    '''Get a dict representing the graph store in a file'''
    return nx.to_dict_of_lists(bin2graph(filename))

# Tweet import/export functions
def tweets2bin(tweets):
    '''Write a tweet to a file using pickle'''
    pass

def bin2gen(filename):
    '''Get a generator (OwO) of stored tweets'''
    # Remember to use yield for each tweet
    pass
