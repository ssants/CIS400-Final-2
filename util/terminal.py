# Terminal functions
from time import time, strftime, gmtime, sleep
from threading import Thread
try:
    from blessings import Terminal
    term = Terminal()
except:
    term = None

#TODO 
#     Figure out nice way to print headers
#       without having to rewrite uptime/rate counter
#       Maybe just call uptimeheader for each?
#     Also check to see how to ensure proper move_y
#       for each type of header.
#     There will eventually be a header for 
#       miner, centrality, and sentiment analysis;
#     Figure out what info should be printed for each
#     Stoppable thread?

def init_term():
    '''Enables nice printing using blessings if installed'''
    if term: print(term.clear)

sep = '#'*75
def print_header(start=0, func=uptime_h):
    '''Print a header at the top of the terminal'''
    if not term: return
    print(term.clear)
    print(term.move_y(3))
    header = Thread(target=func, args=(term, start), daemon=True)
    header.start()

def get_time(t, fmt="%H:%M:%S"):
    return strftime(fmt, gmtime(t))

def start_countdown(duration=60*15):
    global countdown
    countdown = duration

def uptime_header(func):
    '''
    A decorator for all headers
    Each header uses uptime as the first line,
    then can add more lines after.
    All func has to do is print whatever it wants 
    '''
    def header(term, start):
        while True:
            with term.location():
                print(term.move_y(0) + term.clear_eol, end='')
                print('Uptime:', get_time(time()-start), end='')
                if countdown:
                    print(' | Rate Timeout:', get_time(countdown))
                    countdownw -= 1
                    if countdown = -1: countdown = None
                else:
                    print()
                func(term, start)
                print(sep)
            sleep(1)
    return header

# For centrality
def centrality_h(term, start):
    '''Prints uptime and other info when running centrality'''
    global node_count, queue_count, iteration, countdown
    node_count = queue_count = iteration = 1
    countdown = None
    # Print every second using the top 3 lines
    print(term.move_y(0) + term.clear_eol, end='')
    print('Stream uptime:', get_time(time() - start), end='')
    if countdown:
        print(' | Rate Timeout:', get_time(countdown))
        countdown -= 1
        if countdown == -1: countdown = None
    else:
        print()
    print(term.clear_eol, end='')
    print('Total nodes = {} | Next queue = {} | Iteration #{}'
            .format(node_count, queue_count, iteration))

def update_header(nc=None, qc=None, it=None):
    global node_count, queue_count, iteration, countdown
    if nc: node_count = nc
    if qc: queue_count = qc
    if it: iteration = it

