#!/usr/bin/python3
from twitter.stream import TwitterStream, Timeout, HeartbeatTimeout, Hangup
import time
import json
from util.twitter import oauth_login, get_info
from util.db import TweetDB
from pathlib import Path

# TODO:
# - make a limit parameter? right now it runs forever
#   - maybe "press f to stop"?
# - graceful shutdown (take care of stream closing)

bidenTerms = ['joebiden', 'teamjoe', 'biden2020', 'joe2020']
bernieTerms = ['sensanders','berniesanders', 'feelthebern', 'bernie2020']
warrenTerms = ['senwarren', 'ewarren', 'winwithwarren', 'warren2020']
peteTerms = ['petebuttigleg', 'buttigleg', 'pete2020', 'buttigleg2020']
harrisTerms = ['kamalaharris', 'harris2020', 'kamala2020']

def main(db=None, track=None, table=None):
    # Database init
    if not db:
        db = str(input("> Enter an existing file to open or new file to create one: "))
    p = Path(db)
    if p.is_file():
        print(f"Opening existing database {p.name}")
    else:
        print(f"Creating new database {p.name}")
    tdb = TweetDB(db=str(p.resolve()))
    # Table init
    if not table:
        table = str(input("> Enter the table to write to: "))
    tdb.add_table(table)
    print(f"Writing to table \"{table}\"")

    # Twitter init
    auth = oauth_login()
    stream = TwitterStream(auth=auth)
    # track needs to be csv
    if type(track) is list: 
        track = ','.join(track)
    elif type(track) is not str:
        track = str(input("> Enter comma-separated words (e.g.: \"otto,orange\") to track (or nothing to skip):\n> "))

    # Start mining
    if track == "":
        print("Sampling the entire Twitter stream (no track)")
        tw_iter = stream.statuses.sample(language='en')
    else:
        print(f"Tracking {track}")
        tw_iter = stream.statuses.filter(track=track, language='en')

    for tw in tw_iter:
        if tw in [None, Timeout, HeartbeatTimeout, Hangup]:
            print("-- Something happened --")
            break
        elif tw['is_quote_status']:
            # Not sure how to count quoted statues yet
            continue
        else:
            print("*", end='', flush=True)
            tdb.indate_tweet(table, *get_info(tw))

if __name__ == "__main__": main()
