# Predicting the Outcome of the 2020 Primaries using Twitter

This project was originally created to predict the outcome of the 2020 Democratic primaries by mining Twitter.
The two methods used are sentiment analysis, to get a sentiment score for each tweet mined,
and a centrality analysis, to try to weigh tweets based on the author's influence.

#### To Do

* get data and generate some graphs
* pretty printing with `blessings`
* overall cleanup and optimization of code

#### Installation
Since this project relies on accessing Twitter data, a 
[Twitter Developer](https://developer.twitter.com/en/apps)
account is required. 
For an in depth guide on the Twitter API including how to
create an account, read the [Twitter API doc](./docs/twitter_api.md)

The three main programs, `miner.py`, `sentiment_analysis.py` and `centrality.py` 
make use of a wide variety of Python libraries that need to be installed:

* `twitter`, a minimalist wrapper for the Twitter API
* `tweepy`, an easy-to-use wrapper for the Twitter API (this will be replaced in favor of `twitter`)
* `networkx`, a library for creating, manipulating and analyzing a wide variety of graphs
* `matplotlib`, a plotting package
* `nltk`, the natural language processing toolkit
* `textblob`, a simple API for processing text that plays nicely with `nltk`
* optional: `blessings`, a library that make working in terminals easier

Additionally, `nltk` needs to download some files, so fire up the Python interpreter and run:

```python
import nltk
nltk.download('twitter_samples')
nttk.download('stopwords')
```

You should now be able to run any of the programs here. 
Read on further for more details on each program.

## Mining Tweets with `miner.py`

To run the miner:

```python
import miner
# miner.main(terms, filename)
miner.main(bernieTerms, 'bernie.bin')
```

The miner takes two arguments: 

* `terms`, a list of words to track and 
* `filename`, a file to output to

The miner runs indefinitely, so sit back and relax.

Originally, only the top five candidates for the Democratic party were analyzed. 
To help with mining, the following lists of keywords have been pre-defined in the miner:

| List        | Candidate        | Keywords |
| ----------- | ---------------- | -------- |
| `bidenTerms`  | Joe Biden        | joebiden, teamjoe, biden2020, joe2020 |
| `bernieTerms` | Bernie Sanders   | sensanders, berniesanders, feelthebern, bernie2020 |
| `warrenTerms` | Elizabeth Warren | senwarren, ewarren, winwithwarren, warren2020 |
| `peteTerms`   | Pete Buttigleg   | petebuttigleg, buttigleg, pete2020, buttigleg2020 |
| `harrisTerms` | Kamala Harris    | kamalaharris, harris2020, kamala2020 |

Any of these lists can be provided as `terms` when runing the miner.

## Sentiment Analysis with `sentiment_analysis.py`

For sentiment analysis the large majority of the code was to set up the classifier.
This included tokenizing the tweet, removing stop words, and normalizing/lemmatizing each word.
All of this is put togethere in the Sent_Analysis method, which returns a tuple of `int`s:

```python
return (counter_sent_neg, counter_sent_pos)
```

where `counter_sent_neg` is the number of negative tweets in the data set and `count_sent_pos` is
the number of positive tweets in the data set.

The simplest way to use `sentiment_analysis.py` is to edit the `FILENAME` variable at the top to
point to the data set to analyze. Then just run:

```python
import sentiment_analysis
sentiment_analysis.main()
```

## Centrality Analysis with `centrality.py`

The `centrality.py` program has a couple of constants defined near the top, including
`SEED`, the user name to start at, and a couple limits like `NODE_LIMIT`, the max number
of nodes to include and `MAX_DEPTH`, the number of levels to traverse.
There is also the `MODE` constant that should either be 
`'crawl'` to get a network from a user's followers or
`'create'` to make a network from a user's friends (who they follow).
The default mode is `'crawl'` with a `MAX_DEPTH` of 3.

There are a few functions that do separate things:

* `create_network(seed)`: create a bottom-up network starting from user `seed` by adding friends recursively
* `crawl_network(seed)`: get a top-down network starting from user `seed` by adding followers recursively
* `analyze_network(g)`: run some analysis (such as Katz centrality) on a `networkx` graph
* `get_centrality_score(user,g)`: returns the Katz centrality of a user given their network
* `graph_network(g)`: graph a given network using `matplotlib`

You can also just call `main` to run the program automatically with the defined constants.

One final note: the graph of the network is stored as a `dict` of `list` into `nodes.bin`
and an image of the network is saved as `network.png` for use later.

```python
import centrality
# Either use
centrality.main()
# or use
centrality.create_network(seed='realdonaldtrump')
```

## Misc

The all helper functions will eventually be moved to `utility.py`

The final program, `displayer.py` was used to create and display 
graphs using the final sentiment score. 
It also includes the option to run sentiment analysis if 
not done yet and then create graphs.

##

This was originally my group's final project for CIS 400: Principles of Social Media and Mining (Spring 2019).
The original group's project is located [here](https://gitlab.com/ssants/CIS400-Final).
