#!/usr/bin/python3
from nltk.tokenize import TweetTokenizer
from nltk.corpus  import stopwords, wordnet, twitter_samples
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag, classify
from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier
import string
from util.db import TweetDB
from pathlib import Path

# TODO:
# - check if we can download stopwords, twitter_samples automatically
# - check if we can save the trained classifier?
# - machine learning for classifier
# - issue: tweets talking about two candidates
# - issue: contractions

testDataSet = ["Deep Dreaming Bob Ross Cat: The Library of my new Dutch management movement: Human desires. Cost!",
               "It's only a gambling addiction if you keep losing, otherwise it's a high paying career.",
               "Mark Zukerberg used to be a hero of the digital age, but now he has lived long enough to see himself become the villain",
               "I'm happy for you",
               "I loath you with every fiber of my being"
              ]

testDataSet2 = ["It's only a gambling addiction if you keep losing, otherwise it's a high paying career.",
                "#FollowFriday @France_Inte @PKuchly57 @Milipol_Paris I'm being top engaged members in my community this week :)"
               ]



### Data Preprocessing Section ###

# 1. Tokenization
ttk = TweetTokenizer(
        preserve_case=False, # converts to lower (except emoticons)
        reduce_len=True, # cut down 'waaaaayyyyy' to 'waaayyy'
        strip_handles=True) # remove @ handles

def tokenize_str(txt):
    """Convert a string to a list of tokens"""
    tokens = ttk.tokenize(txt)
    return tokens

# 2. Filter tokens
stop_words = stopwords.words('english') # All stopwords in English

def filter_tokens(tokens):
    """Remove punctuation and stop words from tokens"""
    new_tokens = set()
    for t in tokens:
        t = t.strip(string.punctuation)
        if t not in stop_words and t != '':
            new_tokens.add(t)
    return list(new_tokens)

# 3. Normalization
wnl = WordNetLemmatizer()

def tag_converter(tag): 
    """Helper function to look up part-of-speech tags"""
    pos = None
    if tag.startswith('J'):
        pos = wordnet.ADJ
    elif tag.startswith('V'):
        pos = wordnet.VERB
    elif tag.startswith('N'):
        pos = wordnet.NOUN
    elif tag.startswith('R'):
        pos = wordnet.ADV
    return pos

def normalize_tokens(tokens):
    """Normalize tokens by PoS tagging and lemmatization"""
    # Contractions and user mentions (@not_ctrl -> not_ctrl) might be a problem
    new_tokens = []
    # Get a list of tuples=(word, wordnet pos)
    tk_pos = [ (w[0], tag_converter(w[1])) for w in pos_tag(tokens) ] 
    for word, pos in tk_pos:
        if not pos: continue
        w = wnl.lemmatize(word, pos)
        new_tokens.append(w)
    return new_tokens

### Classifier ###
def train_data():
    """Returns a Bayes classifier trained on twitter corpus"""
    print("Training the classifier...")
    pos_tweets = [ (s, 'pos') for s in twitter_samples.strings('positive_tweets.json') ]
    neg_tweets = [ (s, 'neg') for s in twitter_samples.strings('negative_tweets.json') ]
    test_set   = pos_tweets[:100] + neg_tweets[:100]
    train_set  = pos_tweets[100:300] + neg_tweets[100:300]
    classifier = NaiveBayesClassifier(train_set)
    print("Accuracy of classifer: "+str(classifier.accuracy(test_set)))
    return classifier

classifier = train_data()

### Sentiment Analysis ###
def sentiment_analysis(text):
    """Run sentiment analysis on text using TextBlob"""
    tk = tokenize_str(text)
    tk = filter_tokens(tk)
    tk = normalize_tokens(tk)
    tblob = TextBlob(' '.join(tk), classifier=classifier)
    return tblob.sentiment[0]

def row_senti(row):
    """A sentiment analysis wrapper for sqlite rows"""
    # 0 id, 1 content, 2 count, 3 senti
    senti = sentiment_analysis(row[1])
    # Return tuple=(id, sentiment score, weighted score)
    return (row[1], senti, senti*(row[2]+1))

### Main ###
def main(db_name=None, table=None):
    # Check required args
    if not db:
        db_name = str(input("> Enter an existing database: "))
    while True:
        p = Path(db_name)
        if p.is_file():
            db = TweetDB(str(p.resolve()))
            # check if valid db opened
            if isinstance(db.connected, bool):
                break
        else:
            print("!! Not a file")
        db_name = str(input("> Enter an existing database: "))
    tables = db.list_tables()
    if not table:
        print(f"> Tables in {p.name}:")
        for t in tables:
            print(f">  * {t}")
    while table not in tables:
        table = str(input("> Pick a table to analyze: "))

    # Do sentiment analysis
    update_list = []
    # Run sentiment analysis on chunks at a time
    for rows in db.read_chunk(table):
        update_list = list(map(sentiment_analysis, rows))
        db.batch_senti(table, update_list)


def test(db_name='tweets.db', table='Tweets'):
    """Run a demo of the sentiment analysis"""
    db = TweetDB(db=db_name)
    print("Press 'q' to exit, <Enter> to continue")
    for c in db.read_chunk(table, size=1):
        t = row_senti(c[0])
        print(f"\"{t[0]}\"")
        print()
        print(f"Sentiment Score: {t[1]}")
        print(f"Weighted Score:  {t[2]}")
        i = input()
        if i == 'q': break

if __name__ == '__main__': main()
